import React, { Component } from "react";
import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity, ScrollView, ImageBackground } from "react-native";
import { Icon, TabBar, Grid } from '@ant-design/react-native';
import axios from "axios";

class Home extends Component {
    state = {
        items: []
    };

    goToProfile = () => {
        this.props.history.push('/profile')
    }
    onClickMovie = (item) => {
        this.props.history.push('/moviedetail', { item: item })

    }

    onClickBooking = (item) => {
        this.props.history.push('/booking', { item: item })

    }



    componentDidMount() {
        this.getMovie()
    }
    getMovie = () => {
        axios.get('https://zenon.onthewifi.com/ticGo/movies')
            .then(response => {
                this.setState({
                    items: response.data,
                    isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    render() {

        return (
            <TabBar
                unselectedTintColor="white"
                tintColor="#AC3333"
                barTintColor="#AC3333"
            >
                <TabBar.Item
                    title="MOVIES"
                    icon={<Icon name="video-camera" />}
                >
                    
                    <ImageBackground source={require('./profile-bg.jpg')} style={styles.Background}>
                            <View style={styles.header}>
                                <Text style={{ color: 'white', fontSize: 30, textAlign: 'center' }}>Movies</Text>
                            </View>
                            <ScrollView>
                                {!this.state.isLoading ? (
                                    <View style={[styles.boxContent]}>

                                        <Grid
                                            data={this.state.items}
                                            columnNum={2}
                                            renderItem={(item) => (
                                                <TouchableOpacity
                                                    onPress={() => this.onClickMovie(item)}
                                                >
                                                    <Image source={{ uri: item.image }}
                                                        style={{ width: '100%', height: 250 }} />
                                                    <Text style={[styles.textHead]}>{item.name}</Text>
                                                </TouchableOpacity>
                                            )}
                                            itemStyle={{ height: 270, margin: 10 }}
                                        />
                                    </View>
                                ) : (
                                        <ActivityIndicator size="large" color="#0000ff" style={[styles.center]} />
                                    )}
                                <View style={{ padding: 30 }}>
                                    <View style={[styles.rowHeader]}>
                                    </View>
                                    
                                </View>
                                
                            </ScrollView>
                        
                        </ImageBackground>
                </TabBar.Item>


                <TabBar.Item
                    icon={<Icon name="heart" />}
                    title="MY MOVIES"
                />

                <TabBar.Item
                    icon={<Icon name="reload-time" />}
                    title="BOOKING"
                    selected={this.state.selectedTab === 'booking'}
                  onPress={() => this.onClickBooking('booking')}

                />


                <TabBar.Item
                    icon={<Icon name="user" />}
                    title="PROFILE"
                    selected={this.state.selectedTab === 'profile'}
                    onPress={this.goToProfile}
                />
            </TabBar>

        )
    }
}
const styles = StyleSheet.create({

    Background: {
        width: '100%',
        height: '100%',
        
    },

    container: {
        flex: 1,
        
    },

    header: {
        backgroundColor: '#AC3333',
        flex: 0.1,
        flexDirection: 'column',
        borderBottomWidth: 3,
        borderColor: 'white',
        justifyContent: 'center',
        padding: 20,

    },

    content: {
        flex: 1,
        flexDirection: 'column'

    },

    boxContent: {
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        justifyContent: 'center',
        margin: 10

    },

    row: {
        backgroundColor: '#85C1E9',
        flex: 1,
        margin: 14,
        flexDirection: 'row'
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        
    },
    rowHeader: {
        flex: 1,
    },
    footer: {
        backgroundColor: '#AC3333',
        flex: 0.1,
        flexDirection: 'row',
        borderBottomWidth: 3,
        justifyContent: 'center',
        padding: 30,
    },
    box1: {
        flex: 1,
        margin: 14,
        alignItems: 'center',
        justifyContent: 'center'
       
    },

    box: {
        flex: 1,
        margin: 1,
        alignItems: 'center',
        justifyContent: 'center'
      
    },

    logoSize: {
        borderRadius: 10,
        width: 30,
        height: 30
    },


})

export default Home


