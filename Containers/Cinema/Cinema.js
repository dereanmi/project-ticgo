import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList, Dimensions } from 'react-native';
import { TabBar, Icon, ActivityIndicator, Button, Tabs, Card } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import axios from 'axios';
import seatSofa from './sofa-seat.png'
import seatSofa_check from './user-reserve.png'

var options = { year: 'numeric', month: 'long', day: 'numeric' }

class Cinema extends Component {

    state = {
        movie: [],
        seats: [],
        booking: [],
        check: true,
        total: [],
        new: []
    }

    UNSAFE_componentWillMount() {
        const item = this.props.location.state.item
        console.log('type seattt: ', item)
       
        this.setState({ movie: this.props.location.state.item })
      
        this.renderSeat();
        this.setState({ seats: item.seats })
        

    }

    

    onClickBack = () => {
        this.props.history.push('/Home')

    }


    renderImage = () => {
        var imgSource = this.state.check ? seatSofa : seatSofa_check
        return (
            <View>
                <Image
                    source={imgSource}
                />
            </View>
        )
    }

    renderOrderSeat = (item) => {
        if (item.item.type === "SOFA") {
            return (
                <View style={styles.detailSeat}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={styles.detail}>
                                <Image style={styles.seat3} source={require('./sofa-seat.png')} />
                                <Text style={styles.textDetailSeat}>{item.item.name}</Text>
                                <Text style={styles.textDetailSeat}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else if (item.item.type === "PREMIUM") {
            return (
                <View style={styles.detailSeat}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={styles.detail}>
                                <Image style={styles.seat1} source={require('./premium-seat.png')} />
                                <Text style={styles.textDetailSeat}>{item.item.name}</Text>
                                <Text style={styles.textDetailSeat}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={styles.detailSeat}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={styles.detail}>
                                <Image style={styles.seat1} source={require('./ordinary-seat.png')} />
                                <Text style={styles.textDetailSeat}>{item.item.name}</Text>
                                <Text style={styles.textDetailSeat}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        }
    }

    checkSeat = (item) => {
        if (item === false || this.state.seat === false) {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={styles.seat3B} source={require('./sofa-seat.png')} />
            )
        } else if (item === true) {
            return (
                <Image style={styles.seat3B} source={require('./user-reserve.png')} />
            )
        } else if (this.state.seat === true) {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={styles.seat3B} source={require('./sofa-seat.png')} />
            )
        }
        else {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={styles.seat3B} source={require('./user-reserve.png')} />
            )
        }
    }

    onPressSeat = (type, row, column, price, items) => {
        
        let item = this.state.new
        item = item.map(i => {
            if (i.type !== type) {
                return i
            }
            return Object.keys(i).reduce((sum, each, indexRows) => {
                sum[each] = i[each]
                if (each.includes('row') && !each.includes('rows')) {
                    sum[each] = sum[each].map((item, indexCulumn) => {
                        if (each === `row${(row + 1)}` && indexCulumn === column && item === 'F') {
                            this.state.total.push(price)
                            return 'C'
                        }
                        else {
                            if (each === `row${(row + 1)}` && indexCulumn === column && item === 'C') {

                                return 'F'
                            } else {
                                return item
                            }
                        }
                    })
                    
                }
              
                return sum
            }, {})
        })
        console.log('POOM2: ', item);
        this.setState({ new: item })
        console.log('Booking,:', this.state.booking)

        if (items === 'F') {
            this.setState({
                booking: [...this.state.booking, {
                    type: type,
                    row: (row + 1),
                    column: (column + 1)
                }]
            })
        } else if (items === 'C') {
            let result = this.state.booking.filter((item) => {
                return !(item.row === row && item.column === column && item.type === type)
            })
            console.log('resultbookingChack:', result);
            this.setState({ booking: result })
        }
        console.log('Booking,:', this.state.booking)
    }


        
        
    

    renderSeat = () => {
        console.log(this.props.location.state)
        let item = this.props.location.state.item.seats
        
        item = item.map(i => {
            return Object.keys(i).reduce((sum, each) => {
                sum[each] = i[each]
                if (each.includes('row') && !each.includes('rows')) {
                    sum[each] = sum[each].map(item => item === true ? 'B' : 'F')
                    console.log();
                }
                return sum
            }, {})
        })
        console.log('POOM2: ', item);
        this.setState({ new: item })
    }

    Booking = () => {
        const { user } = this.props
        console.log("1pppp", user)
        console.log("2", this.state.movie._id)
        console.log("3", this.state.booking);

        axios({
            method: 'post',
            url: 'https://zenon.onthewifi.com/ticGo/movies/book',
            headers: { 'Authorization': `bearer ${user.token}` },
            data: {
                showtimeId: this.state.movie._id,
                seats: this.state.booking
            }
        })
            .then(() => {
                alert('Booking is Success')
            })
            .catch((error) => {
                console.log('MMMMMMM',error.response);
                alert(error.response)
            })
    }

    _keyExtractorSeats = (item, index) => item._id

    _keyExtractorDetailSeat = (item, index) => item._id

    render() {
        console.log("SUSk", this.state.movie)
        const item = this.props.location.state.item
        console.log('itemM : ', item)
        const movie = this.props.location.state.item.movie
        var date = new Date(item.startDateTime)
        timeStart = date.toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        var date2 = new Date(item.endDateTime)
        timeEnd = date2.toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        date = date.toLocaleDateString("th-TH", options)
        var cinema = this.props.location.state.item.cinema.name
        console.log('Test222: ', this.state.new)
        console.log('Booking,:', this.state.booking)
        return (
            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <TouchableOpacity
                        style={styles.back}
                        onPress={this.onClickBack}
                    >
                        <Icon name="left" size="md" color="white" />
                    </TouchableOpacity>

                    <View style={styles.icon}>
                        <Text style={{ color: 'white',fontSize: 20 }}>{movie.name}</Text>
                    </View>
                    <View style={styles.back}>
                    </View>
                </View>
                <View style={styles.HeaderContent}>
                    <View style={styles.detailMovie}>
                        <Image
                            source={{ uri: movie.image }}
                            style={{ width: '20%', height: '100%', resizeMode: 'contain' }}
                        />
                        <View style={styles.infoMovie}>
                            <Text style={{ color: 'black', margin: 3, fontSize: 15 }}>{date}</Text>
                            <Text style={{ color: 'black', margin: 3, fontSize: 15 }}><Icon name="clock-circle" size="md" color="#AC3333" /> {timeStart} - {timeEnd}</Text>
                            <Text style={{ color: 'black', margin: 3, fontSize: 15 }}>{cinema} | &nbsp;<Icon name="sound" size="xxs" color="#AC3333" /> {item.soundtrack}</Text>
                        </View>
                    </View>
                </View>
                <FlatList
                    keyExtractor={this._keyExtractorDetailSeat}
                    numColumns={item.seats.length}
                    data={item.seats}
                    renderItem={(item) => (
                        this.renderOrderSeat(item)
                    )}
                />
                <ScrollView>
                    <View style={styles.content}>
                       
                        {
                            this.state.new.map((i, index) => {
                                console.log('i: ', i)
                                return Object.keys(i).map((rowsData, indexRows) => {
                                    
                                    console.log('rowsData: ', rowsData)
                                    
                                    return (
                                        <FlatList
                                            keyExtractor={this._keyExtractorSeats}
                                            data={i['row' + (indexRows + 1)]}
                                            numColumns={i.columns}
                                            extraData={this.state}
                                            renderItem={({ item, index }) => {
                                                console.log('eieiei', i.type)
                                               

                                                if (item === 'F') {
                                                    if (i.type === "SOFA") {
                                                        return (
                                                            <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price) }}>
                                                                <Image style={styles.seat3B} source={require('./sofa-seat.png')} />
                                                            </TouchableOpacity>
                                                        )
                                                    }
                                                    else if (i.type === "PREMIUM") {
                                                        return (
                                                            <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price) }}>
                                                                <Image style={styles.seat1B} source={require('./premium-seat.png')} />
                                                            </TouchableOpacity>
                                                        )
                                                    }
                                                    else if (i.type === "DELUXE") {
                                                        return (
                                                            <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price) }}>
                                                                <Image style={styles.seat1B} source={require('./ordinary-seat.png')} />
                                                            </TouchableOpacity>
                                                        )
                                                    }
                                                }
                                                else if (item === 'C') {
                                                    return (
                                                        <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price) }}>
                                                            <Image style={styles.seat1B} source={require('./user-reserve.png')} />
                                                        </TouchableOpacity>
                                                    )
                                                }
                                                else if (item === 'B') {
                                                    return <Image style={styles.seat1B} source={require('./user-reserve.png')} />
                                                }
                                            }
                                            }
                                        />
                                    )
                                })
                            }).reverse()
                        }
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: 'black', fontSize: 14, justifyContent: 'flex-end' }}>{this.state.total.reduce((x, y) => { return x + y }, 0)} THB</Text>
                        </View>
                        <View style={{ width: '100%' }}>
                            <Button type='primary' onPress={() => { this.Booking() }}>Book</Button>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    boxHeader: {
        backgroundColor: '#AC3333',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    back: {
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    HeaderContent: {
        flexDirection: 'column',
        justifyContent: 'center',
        margin: 10
    },
    detailMovie: {
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    detailSeat: {
        flex: 1,
        backgroundColor: '#34444b',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    infoMovie: {
        margin: 5,
        marginLeft: 10
    },
    seat1: {
        width: 15,
        height: 15
    },
    seat3: {
        width: 40,
        height: 15
    },
    seat1B: {
        width: 10,
        height: 10,
        margin: 1
    },
    seat3B: {
        width: 25,
        height: 8,
    },
    user: {
        width: 8,
        height: 8,
    },
    textDetailSeat: {
        color: 'white',
        fontSize: 8
    },
    detail: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    screen: {
        width: '100%',
        height: 50,
        borderRadius: 20,
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, { push })(Cinema)
