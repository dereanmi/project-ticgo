import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator, ListItem, Alert, FlatList } from 'react-native';
import { Icon, SearchBar, TabBar, Tabs, List, Flex, Grid, ListView, Button, Carousel, Card, WhiteSpace } from '@ant-design/react-native';
import { push } from 'connected-react-router'
import axios from 'axios'
import { connect } from 'react-redux'

class booking extends Component {
    state = {
        history: [],
        tickets: [],
        ticket: [],
        selectedTab: 'Movie'
    }

    goToLogin = () => {
        this.props.history.push('/Login')
    }
    goToProfile = () => {
        this.props.history.push('/Profile')
    }
    onClickBack = () => {
        this.props.history.push('/Movie')

    }

    UNSAFE_componentWillMount = () => {

        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/movies/book', {
            headers: { 'Authorization': `bearer ${user.token}` }
        })
            .then(res => {
                console.log('RES:', res.data);
                const dataTickets = res.data
                this.setState({ tickets: dataTickets })
                for (let index = 0; index < dataTickets.length; index++) {
                    console.log('Ticket INdex?:', dataTickets[index]);
                    axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${dataTickets[index].showtime}`)
                        .then(res => {
                            this.setState({ ticket: [...this.state.ticket, res.data] })
                            console.log('ticket:', this.state.ticket);
                            console.log('ticketS:', this.state.tickets);
                        })
                        .catch(error => {
                            console.log('ERROR_showtime:', error);
                        })
                }
            })
            .catch(error => {
                console.log('ERROR:', error);
            })

    }

    seatPrint = (seats) => {
        const seat = []
        for (let index = 0; index < seats.length; index++) {
            seat.push(<Text>{seats[index].type} Column: {seats[index].column} Row: {seats[index].row}</Text>)
        }
        return seat
    }

    addZero(time) {
        if (time < 10) {
            time = "0" + time;
        }
        return time;
    }


    render() {
        return (
            <View style={[styles.container, styles.center]}>
                <View style={styles.boxHeader}>
                    <TouchableOpacity
                     
                        onPress={this.onClickBack}
                    >
                        <Icon name="left" size="lg" color="white" />
                    </TouchableOpacity>

                    <View style={styles.icon}>
                        <Text style={styles.text}>BOOKING HISTORY</Text>
                    </View>
                
                </View>
                
                <View style={{ margin: 5 }}>
                    <ScrollView style={{ flex: 1, backgroundColor: '#AC3333' }}>
                        <FlatList
                            style={{ flex: 1 }}
                            inverted data={this.state.ticket}
                            renderItem={({ item, index }) => (
                                <Card style={{ backgroundColor: 'white', margin: 5 }}>
                                    <Card.Header
                                        title={item.name}
                                        extra={item.movie.duration + ' min'}
                                    />
                                    <Card.Body style={{ flexDirection: 'row' }}>
                                        <View style={{ flexDirection: 'row', margin: 4 }}>
                                            <Image style={{ width: '50%', height: 110 }} source={{ uri: item.movie.image }} resizeMode="contain" />
                                        </View>
                                        <View >
                                            <Text>{new Date(this.state.tickets[index].createdDateTime).toLocaleDateString()}</Text>
                                            {item.index}{this.seatPrint(this.state.tickets[index].seats)}
                                            <Text> Show time : {this.addZero(new Date(item.startDateTime).toLocaleDateString())} </Text>
                                            <Text> {this.addZero(new Date(item.startDateTime).getHours())} : {this.addZero(new Date(item.startDateTime).getMinutes())}</Text>
                                        </View>
                                    </Card.Body>
                                </Card>
                            )}
                        />
                    </ScrollView>
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    center: {
        alignItems: 'center',
        // justifyContent: 'center'
    },
    text: {
        textAlign: 'center',
        fontSize: 20,
        color: 'white',
    },
    boxHeader: {
  
        backgroundColor: '#AC3333',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        // padding: 15,
        // margin: 10

    },
    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // justifyContent: 'flex-start',
    },
});
const mapStatetoProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStatetoProps, null)(booking)

