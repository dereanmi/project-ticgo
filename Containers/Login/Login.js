import React, { Component } from 'react';
import { Text, View, ImageBackground, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Button, InputItem, WhiteSpace } from '@ant-design/react-native';
import { Link } from 'react-router-native'
import {connect} from 'react-redux'
import axios from 'axios';

class Login extends React.Component {

    state = {
        username: '',
        password: '',
        email: '',
        firstname: '',
        lastname: ''
    };

    goToHome = () => {
        this.props.history.push('/home', {
        })
    }

    goToRegister = () => {
        this.props.history.push('/register', {
        })
    }


    onChangeValue = (index, value) => this.setState({ [index]: value })
    onLogin = () => {
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users/login?email',
            method: 'post',
            data: {
                // email: this.state.email,
                // password: this.state.password,
                email: 'dream@gmail.com',
                password: '1234'
            }
        }).then(res => {
            const { data } = res
            const { user } = data
            this.props.addUser(data);
            this.props.history.push('/Home')
        }).catch(e => {
            console.log("error " + e)
            alert(e.response.data.errors.email)
        })
    }


    render() {
        const { username, password } = this.state
        return (

            <ImageBackground source={require('./bg.jpg')} style={styles.Background}>
                <View style={[styles.container, styles.transparent]} >
                    <View style={[styles.content]}>
                        <View style={[styles.layout1, styles.centerLayout1]}>
                            <Text style={styles.textStyle}>
                                TICGO BOOKING
                            </Text>
                            <View style={styles.center}>
                                <Image source={require('./profile-icon.png')} style={styles.profile} />
                            </View>
                        </View>
                        <View style={[styles.layout2, styles.centerLayout2]}>


                            <InputItem
                                clear
                                value={this.state.email}
                                onChange={email => {
                                    this.setState({
                                        email,
                                    });
                                }}
                                placeholder="๊Username"
                            >
                                <Image source={require('./username-icon.png')} style={styles.usernameIcon} />
                            </InputItem>
                            <WhiteSpace></WhiteSpace>
                            <InputItem
                                clear
                                value={this.state.password}
                                onChange={password => {
                                    this.setState({
                                        password,
                                    });
                                }}
                                placeholder="๊Password"
                            >
                                <Image source={require('./password-icon.png')} style={styles.passwordIcon} />
                            </InputItem>

                            <WhiteSpace></WhiteSpace>
                            <View style={styles.loginmiddle}>
                                <WhiteSpace></WhiteSpace>
                                <View style={styles.loginButton}>
                                    <TouchableOpacity onPress={this.onLogin}>
                                        <Text style={styles.textButton}>Login</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.loginFooter}>
                                <View>
                                    <TouchableOpacity onPress={this.goToRegister}>
                                        <Text style={{ fontSize: 17, color: 'black' }}>Register</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </View >

            </ImageBackground>

        )
    }
}

const styles = StyleSheet.create({

    Background: {
        width: '100%',
        height: '100%',

    },

    container: {
        flex: 1,
    },

    content: {
        flex: 1,
        flexDirection: 'column'

    },
    layout1: {

        flex: 0.9,
        flexDirection: 'column'
    },

    layout2: {

        flex: 1,
        flexDirection: 'column'
    },

    loginmiddle: {
        flex: 0.5,
        flexDirection: 'row'
    },

    loginFooter: {
        flex: 0.5,
        flexDirection: 'row'
    },

    profile: {
        borderRadius: 50,
        width: 100,
        height: 100
    },

    usernameIcon: {
        borderRadius: 10,
        width: 20,
        height: 20
    },

    passwordIcon: {
        borderRadius: 10,
        width: 30,
        height: 30
    },

    loginButton: {
        backgroundColor: '#AC3333',
        flex: 1,
        margin: 20,
        borderRadius: 5,
        width: 100,
        height: 40
    },

    textStyle: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 15
    },

    textButton: {
        color: 'black',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20
    },

    centerLayout1: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout2: {
        alignItems: 'center',
        // justifyContent: 'center'
    },

    transparent: {
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        flex: 1,
        margin: 15
    }

})
const mapStateToProps = ({ user }) => ({
    username: user.username,
    password: user.password,
})

const mapDidpatchToProps = (dispatch) => {
    return {
        addUser: (user) => {
            dispatch({
                type: 'ADD_USER',
                user: user
            })
        }
    }
}

export default connect(
    mapStateToProps,
    mapDidpatchToProps
)(Login)
